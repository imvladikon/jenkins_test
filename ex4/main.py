import urllib.request, urllib.parse
# it's better to use: requests library
import sys
import re

DELIMETER=","

def full_url(url):
    if not re.match('(?:http|ftp|https)://', url):
        return 'http://{}'.format(url)
    return url

def html(url):
    try:
        with urllib.request.urlopen(url) as response:
            return response.read()
    except:
        print("error with fetching {}".format(url))

def main(args):
    for _ in args:
        url = full_url(_.strip())
        print("fetching {}".format(url))
        html_content = html(url)
        if not html_content:
            # print("error fetching {}".format(url))
            continue
        print(html_content)

if __name__ == "__main__":
    args= " ".join(sys.argv[1:]).split(DELIMETER)
    main(args)

