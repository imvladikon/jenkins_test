clearContainers () {
    CONTAINER=$1 || " "
    echo "clearing containers with name starting with: "$CONTAINER
    CONTAINER_IDS=$(docker ps -aqf "name=^$CONTAINER")
    if [ -z "$CONTAINER_IDS" ] || [ "$CONTAINER_IDS" = " " ]; then
        echo "---- No containers available for deletion ----"
    else
        docker container stop $CONTAINER_IDS || true
        docker container rm -f $CONTAINER_IDS || true
    fi
}

ckeckClearContainer() {
    CONTAINER=$1 || " "
    if [ -z "$CONTAINER" ] || [ "$CONTAINER" = " " ]; then
        echo "---- No containers available for deletion ----"
    else
        docker inspect -f '{{.State.Running}}' $CONTAINER \
        && docker container stop $CONTAINER
        docker container rm $CONTAINER || true
    fi
}


firstname=vladimir
lastname=gurevich
docker_image=docker_ex4_${firstname}_${lastname}
docker_container=${docker_image}_instance
docker_folder="ex4"
logs_folder=/tmp/docker_ex_${firstname}_${lastname}
mkdir -p $logs_folder || true
clearContainers $docker_container
docker build -t $docker_image $docker_folder --no-cache
filename=${FILENAME}
counter=0
for URLS in $(cat ${filename}) ; do
  echo "processing $URLS"
  counter=$((counter+1))
  #ckeckClearContainer ${docker_container}_$counter
  docker run -d -e "URLS=${URLS}" --name ${docker_container}_$counter ${docker_image}
  docker logs -f ${docker_container}_$counter > ${logs_folder}/${docker_container}_$counter.log
done
