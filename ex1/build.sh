
clearContainers () {
    CONTAINER=$1 || " "
    echo "clearing containers with name starting with: "$CONTAINER
    CONTAINER_IDS=$(docker ps -aqf "name=^$CONTAINER")
    if [ -z "$CONTAINER_IDS" ] || [ "$CONTAINER_IDS" = " " ]; then
        echo "---- No containers available for deletion ----"
    else
        docker container stop $CONTAINER_IDS || true
        docker container rm -f $CONTAINER_IDS || true
    fi
}


firstname=vladimir
lastname=gurevich
docker_image=docker_ex1_${firstname}_${lastname}
docker_container=${docker_image}_instance
docker_folder="ex1"
logs_folder=/tmp/docker_ex_${firstname}_${lastname}
mkdir -p $logs_folder || true
clearContainers $docker_container
docker build -t $docker_image $docker_folder --no-cache
docker run --name $docker_container $docker_image
docker logs $docker_container > ${logs_folder}/${docker_container}.log
