import sys

def main():
   n = 0
   try:
      n = int(sys.argv[1:2][0])
   except:
      print("please, check argument")
      return
   for i in range(n):
      print("{} {}".format(i, i*i))
      


if __name__ == "__main__":
    main()